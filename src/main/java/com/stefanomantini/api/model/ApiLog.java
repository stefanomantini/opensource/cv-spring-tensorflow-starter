package com.stefanomantini.api.model;

import com.stefanomantini.api.enumeration.Message;
import com.stefanomantini.service.contract.ApiTraceService.Direction;
import java.time.Instant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ApiLog {

  private String requestLoggingId;

  private Direction direction;

  private String method;

  private String requestUrl;

  private String correlationId;

  private Message messageType;

  private String messageText;

  private Integer responseStatus;

  private Instant loggedTime;

  private String externalCorrelationId;
}
