package com.stefanomantini.api.model;

import java.time.Instant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class ExecutionStatistic {

  private String declaringClassName;

  private String methodName;

  private Instant start;

  private Instant end;

  private Long duration;
}
