package com.stefanomantini.api.advice;

import com.stefanomantini.aop.logging.AbstractTimerAspect;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Profile("timer")
public class ApiTimerAspect extends AbstractTimerAspect {

  @Pointcut("within(com.stefanomantini.api.controller..*)")
  public void isController() {}

  @Around("isPublicMethod() && isController())")
  public void logAround(final ProceedingJoinPoint joinPoint) throws Throwable {
    this.timer(joinPoint);
  }
}
