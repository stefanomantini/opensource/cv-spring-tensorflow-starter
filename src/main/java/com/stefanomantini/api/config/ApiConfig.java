package com.stefanomantini.api.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({
  "com.stefanomantini.api.advice",
  "com.stefanomantini.api.contract",
  "com.stefanomantini.api.controller",
  "com.stefanomantini.api.filter"
})
public class ApiConfig {}
