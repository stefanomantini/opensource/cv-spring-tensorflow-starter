package com.stefanomantini.api.filter;

import com.stefanomantini.api.constant.ApiConstants;
import com.stefanomantini.service.contract.ApiTraceService;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingResponseWrapper;

@Component
public class ApiTraceFilter extends OncePerRequestFilter {

  private static final String DEFAULT_CHARSET_NAME = Charset.defaultCharset().name();

  private final List<String> ALLOWED_CONTENT_TYPES = Arrays.asList("application/json");

  private static final Pattern URL_PATTERN = Pattern.compile("/api/.*");

  @Autowired private ApiTraceService apiLoggingService;

  @Override
  protected void doFilterInternal(
      final HttpServletRequest request,
      final HttpServletResponse response,
      final FilterChain filterChain)
      throws ServletException, IOException {

    if (!ALLOWED_CONTENT_TYPES.contains(request.getContentType())) {
      logger.debug("not a JSON payload");
      filterChain.doFilter(request, response);
    } else {
      logger.info("logging json payload");

      final String method = request.getMethod();
      final String requestUrl = getRequestURL(request);
      final String traceId = request.getHeader(ApiConstants.HEADER_TRACE_ID);

      final CachedPayloadRequestWrapper cachedRequest =
          CachedPayloadRequestWrapper.wrapIfNecessary(request);

      final ContentCachingResponseWrapper cachingResponse =
          new ContentCachingResponseWrapper(response);

      final String requestLoggingId =
          apiLoggingService.logRequest(
              ApiTraceService.Direction.INBOUND,
              method,
              requestUrl,
              traceId,
              StringUtils.defaultIfEmpty(cachedRequest.getBody(), null),
              null);

      filterChain.doFilter(cachedRequest, cachingResponse);

      apiLoggingService.logResponse(
          ApiTraceService.Direction.INBOUND,
          requestLoggingId,
          method,
          requestUrl,
          traceId,
          extractBody(cachingResponse),
          cachingResponse.getStatusCode());

      cachingResponse.copyBodyToResponse();
    }
  }

  @Override
  protected boolean shouldNotFilter(final HttpServletRequest request) {
    return false; // !URL_PATTERN.matcher(request.getPathInfo()).matches();
  }

  private String getRequestURL(final HttpServletRequest request) {

    final StringBuilder requestURL = new StringBuilder(request.getRequestURL().toString());

    final String queryString = request.getQueryString();
    if (queryString != null) {
      requestURL.append('?').append(queryString);
    }

    return requestURL.toString();
  }

  private String extractBody(final ContentCachingResponseWrapper cachingResponse)
      throws UnsupportedEncodingException {
    return StringUtils.defaultIfEmpty(
        new String(
            cachingResponse.getContentAsByteArray(),
            Optional.ofNullable(cachingResponse.getCharacterEncoding())
                .orElse(DEFAULT_CHARSET_NAME)),
        null);
  }
}
