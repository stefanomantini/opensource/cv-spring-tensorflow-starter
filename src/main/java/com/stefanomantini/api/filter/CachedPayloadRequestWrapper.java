package com.stefanomantini.api.filter;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Optional;
import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import org.apache.commons.io.IOUtils;

class CachedPayloadRequestWrapper extends HttpServletRequestWrapper {

  private static final String DEFAULT_CHARSET_NAME = Charset.defaultCharset().name();

  private final byte[] cachedBytes;

  private final String charset;

  private CachedPayloadRequestWrapper(final HttpServletRequest request) throws IOException {
    super(request);

    /*
     * Eagerly cache the payload - we wouldn't be wrapping the request if we didn't have an
     * immediate need for the content.
     */
    cachedBytes = IOUtils.toByteArray(request.getInputStream());
    charset = Optional.ofNullable(request.getCharacterEncoding()).orElse(DEFAULT_CHARSET_NAME);
  }

  static CachedPayloadRequestWrapper wrapIfNecessary(final HttpServletRequest request)
      throws IOException {

    if (request instanceof CachedPayloadRequestWrapper) {
      return (CachedPayloadRequestWrapper) request;
    } else {
      return new CachedPayloadRequestWrapper(request);
    }
  }

  @Override
  public ServletInputStream getInputStream() throws IOException {

    return new ServletInputStream() {

      private final ByteArrayInputStream bais = new ByteArrayInputStream(cachedBytes);

      @Override
      public int read() throws IOException {

        return bais.read();
      }

      @Override
      public boolean isFinished() {

        return bais.available() == 0;
      }

      @Override
      public boolean isReady() {

        return true;
      }

      @Override
      public void setReadListener(final ReadListener listener) {

        throw new UnsupportedOperationException();
      }
    };
  }

  @Override
  public BufferedReader getReader() throws IOException {

    return new BufferedReader(new InputStreamReader(getInputStream()));
  }

  String getBody() throws UnsupportedEncodingException {

    return new String(cachedBytes, charset);
  }
}
