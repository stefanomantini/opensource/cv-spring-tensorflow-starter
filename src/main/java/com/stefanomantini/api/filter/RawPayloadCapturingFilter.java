package com.stefanomantini.api.filter;

import com.stefanomantini.api.constant.ApiConstants;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
public class RawPayloadCapturingFilter extends OncePerRequestFilter {

  @Override
  protected void doFilterInternal(
      final HttpServletRequest request,
      final HttpServletResponse response,
      final FilterChain filterChain)
      throws ServletException, IOException {

    final CachedPayloadRequestWrapper cachedRequest =
        CachedPayloadRequestWrapper.wrapIfNecessary(request);

    cachedRequest.setAttribute(ApiConstants.ATTRIBUTE_RAW_BODY, cachedRequest.getBody());

    filterChain.doFilter(cachedRequest, response);
  }
}
