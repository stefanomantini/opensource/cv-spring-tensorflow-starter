package com.stefanomantini.api.helper;

import java.io.IOException;
import net.sf.jmimemagic.Magic;
import net.sf.jmimemagic.MagicMatch;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class FileHelper {

  public byte[] checkFile(final MultipartFile file) {
    try {
      this.checkImageContents(file);
      return file.getBytes();
    } catch (final IOException e) {
      return new byte[] {};
    }
  }

  private void checkImageContents(final MultipartFile file) {
    final MagicMatch match;
    try {
      match = Magic.getMagicMatch(file.getBytes());
    } catch (final Exception e) {
      throw new RuntimeException(e);
    }
    final String mimeType = match.getMimeType();
    if (!mimeType.startsWith("image")) {
      throw new IllegalArgumentException("Not an image type: " + mimeType);
    }
  }
}
