package com.stefanomantini.api.controller;

import com.stefanomantini.api.contract.ClassificationContract;
import com.stefanomantini.api.helper.FileHelper;
import com.stefanomantini.service.contract.ClassificationService;
import com.stefanomantini.service.model.ClassificationSpec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class ClassificationController implements ClassificationContract {

  @Autowired private ClassificationService classificationService;

  @Autowired private FileHelper fileHelper;

  @Override
  @PostMapping(value = "/classify")
  public ClassificationSpec classifyImage(@RequestParam final MultipartFile file) {
    return classificationService.classifyImage(this.fileHelper.checkFile(file));
  }
}
