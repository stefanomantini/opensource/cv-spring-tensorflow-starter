package com.stefanomantini.api.contract;

import com.stefanomantini.service.model.ClassificationSpec;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Api(tags = "Classification")
public interface ClassificationContract {

  @ApiOperation(
      value = "Retrieves information about the image",
      notes = "This endpoint will retrieve a label and confidence score about a passed image")
  @ApiResponses({
    @ApiResponse(code = 200, message = "Image classified successfully"),
    @ApiResponse(code = 400, message = "Invalid request, image passed is invalid"),
    @ApiResponse(code = 500, message = "Error while processing the request")
  })
  @ApiImplicitParams({
    @ApiImplicitParam(
        paramType = "header",
        name = "traceId",
        required = false) // TODO change to required
  })
  ClassificationSpec classifyImage(@RequestParam MultipartFile file);
}
