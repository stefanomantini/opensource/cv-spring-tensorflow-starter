package com.stefanomantini.api.enumeration;

enum Direction {
  INBOUND,
  OUTBOUND
}
