package com.stefanomantini.api.enumeration;

public enum Message {
  REQUEST,
  RESPONSE
}
