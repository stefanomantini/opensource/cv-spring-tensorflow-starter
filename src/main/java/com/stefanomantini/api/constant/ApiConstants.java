package com.stefanomantini.api.constant;

public class ApiConstants {
  public static final String ATTRIBUTE_RAW_BODY = "raw-body";
  public static final String HEADER_TRACE_ID = "traceId";
}
