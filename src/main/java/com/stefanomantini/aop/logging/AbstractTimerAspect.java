package com.stefanomantini.aop.logging;

import com.stefanomantini.api.model.ExecutionStatistic;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Pointcut;

import java.time.Instant;

@Slf4j
public abstract class AbstractTimerAspect {

  @Pointcut("execution(public * *(..))")
  public void isPublicMethod() {}

  protected void timer(final ProceedingJoinPoint joinPoint) throws Throwable {
    final Instant start = Instant.now();
    try {
      joinPoint.proceed();
    } finally {
      final Instant end = Instant.now();
      log.info(
          ExecutionStatistic.builder()
              .declaringClassName(joinPoint.getSignature().getDeclaringTypeName())
              .methodName(joinPoint.getSignature().getName())
              .start(start)
              .end(end)
              .duration(end.toEpochMilli() - start.toEpochMilli())
              .build()
              .toString());
    }
  }
}
