package com.stefanomantini;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.env.Environment;

import javax.annotation.PostConstruct;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Slf4j
@EnableAspectJAutoProxy
@SpringBootApplication
public class Application extends SpringBootServletInitializer {

  @Autowired Optional<BuildProperties> buildProperties;

  /**
   * @param args
   * @throws Exception
   */
  public static void main(final String[] args) throws Exception {
    final SpringApplication app = new SpringApplication(Application.class);

    final Environment env = app.run(args).getEnvironment();
    logApplicationStartup(env);
  }

  /** Log build information on startup */
  private static void logApplicationStartup(final Environment env) {

    final String protocol = env.getProperty("server.ssl.key-store") != null ? "https" : "http";

    final String serverPort = env.getProperty("server.port");

    String contextPath = env.getProperty("server.context-path");

    contextPath = StringUtils.isBlank(contextPath) ? "/" : contextPath;

    String hostAddress = "localhost";
    try {
      hostAddress = InetAddress.getLocalHost().getHostAddress();
    } catch (final UnknownHostException e) {
      log.warn("The host name could not be determined, using `localhost` as fallback");
    }

    log.info(
        "\n----------------------------------------------------------\n\t"
            + "Application '{}' is running, Access URLs:\n\t"
            + "Local: \t\t{}://localhost:{}{}/\n\t"
            + "External: \t{}://{}:{}{}/\n\t"
            + "Profile(s): \t{}\n----------------------------------------------------------",
        env.getProperty("spring.application.name"),
        protocol,
        serverPort,
        contextPath,
        protocol,
        hostAddress,
        serverPort,
        contextPath,
        env.getActiveProfiles());
  }

  @PostConstruct
  private void onStart() {
    final Map<String, String> startupInfo = new HashMap<>();
    buildProperties.ifPresent(
        p -> {
          startupInfo.put("Version", p.getVersion());
          startupInfo.put("Build Time", p.getTime().toString());
        });
    startupInfo.entrySet().stream().forEach(e -> logger.info(e.getKey() + ": " + e.getValue()));
  }
}
