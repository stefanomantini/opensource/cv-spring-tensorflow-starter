package com.stefanomantini.service.impl;

import com.stefanomantini.api.enumeration.Message;
import com.stefanomantini.api.model.ApiLog;
import com.stefanomantini.service.contract.ApiTraceService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.UUID;

@Slf4j
@Service
public class ApiTraceServiceImpl implements ApiTraceService {

  private static final String ELLIPSIS = "…";

  private static final int MAX_COLUMN_LENGTH = 4000;

  @Override
  public String logRequest(
      final Direction direction,
      final String method,
      final String requestUrl,
      final String correlationId,
      final String body,
      final String externalCorrelationId) {

    final String uuid = UUID.randomUUID().toString();

    final ApiLog apiCallLogging =
        ApiLog.builder()
            .requestLoggingId(uuid)
            .direction(direction)
            .method(method)
            .requestUrl(truncate(requestUrl, MAX_COLUMN_LENGTH))
            .correlationId(correlationId)
            .messageType(Message.REQUEST)
            .messageText(body)
            .loggedTime(Instant.now())
            .externalCorrelationId(externalCorrelationId)
            .build();

    log.info(apiCallLogging.toString());

    return uuid;
  }

  @Override
  public void logResponse(
      final Direction direction,
      final String requestLoggingId,
      final String method,
      final String requestUrl,
      final String correlationId,
      final String body,
      final Integer responseStatus) {

    final ApiLog apiCallLogging =
        ApiLog.builder()
            .direction(direction)
            .requestLoggingId(requestLoggingId)
            .method(method)
            .requestUrl(truncate(requestUrl, MAX_COLUMN_LENGTH))
            .correlationId(correlationId)
            .messageType(Message.RESPONSE)
            .messageText(body)
            .responseStatus(responseStatus)
            .loggedTime(Instant.now())
            .build();

    log.info(apiCallLogging.toString());
  }

  private String truncate(final String value, final int maxLength) {
    if (value.length() <= maxLength) {
      return value;
    }

    return StringUtils.joinWith(" ", StringUtils.truncate(value, maxLength - 2), ELLIPSIS);
  }
}
