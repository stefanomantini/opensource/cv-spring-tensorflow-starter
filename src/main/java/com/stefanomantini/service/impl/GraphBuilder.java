package com.stefanomantini.service.impl;

import org.tensorflow.DataType;
import org.tensorflow.Graph;
import org.tensorflow.Output;
import org.tensorflow.Tensor;

class GraphBuilder {

  GraphBuilder(final Graph g) {
    this.g = g;
  }

  Output div(final Output x, final Output y) {
    return this.binaryOp("Div", x, y);
  }

  Output sub(final Output x, final Output y) {
    return this.binaryOp("Sub", x, y);
  }

  Output resizeBilinear(final Output images, final Output size) {
    return this.binaryOp("ResizeBilinear", images, size);
  }

  Output expandDims(final Output input, final Output dim) {
    return this.binaryOp("ExpandDims", input, dim);
  }

  Output cast(final Output value, final DataType dtype) {
    return this.g
        .opBuilder("Cast", "Cast")
        .addInput(value)
        .setAttr("DstT", dtype)
        .build()
        .output(0);
  }

  Output decodeJpeg(final Output contents, final long channels) {
    return this.g
        .opBuilder("DecodeJpeg", "DecodeJpeg")
        .addInput(contents)
        .setAttr("channels", channels)
        .build()
        .output(0);
  }

  Output constant(final String name, final Object value) {
    try (final Tensor t = Tensor.create(value)) {
      return this.g
          .opBuilder("Const", name)
          .setAttr("dtype", t.dataType())
          .setAttr("value", t)
          .build()
          .output(0);
    }
  }

  private Output binaryOp(final String type, final Output in1, final Output in2) {
    return this.g.opBuilder(type, type).addInput(in1).addInput(in2).build().output(0);
  }

  private final Graph g;
}
