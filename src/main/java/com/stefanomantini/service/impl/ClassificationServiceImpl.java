package com.stefanomantini.service.impl;

import com.stefanomantini.service.contract.ClassificationService;
import com.stefanomantini.service.model.ClassificationSpec;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.tensorflow.DataType;
import org.tensorflow.Graph;
import org.tensorflow.Output;
import org.tensorflow.Session;
import org.tensorflow.Tensor;

import javax.annotation.PreDestroy;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
public class ClassificationServiceImpl implements ClassificationService {

  private final Session session;

  private final List<String> labels;

  private final String outputLayer;

  private final int width;

  private final int height;

  private final float mean;

  private final float scale;

  public ClassificationServiceImpl(
      final Graph inceptionGraph,
      final List<String> labels,
      @Value("${tf.outputLayer}") final String outputLayer,
      @Value("${tf.image.width}") final int imageW,
      @Value("${tf.image.height}") final int imageH,
      @Value("${tf.image.mean}") final float mean,
      @Value("${tf.image.scale}") final float scale) {
    this.labels = labels;
    this.outputLayer = outputLayer;
    this.height = imageH;
    this.width = imageW;
    this.mean = mean;
    this.scale = scale;
    this.session = new Session(inceptionGraph);
  }

  @Override
  public ClassificationSpec classifyImage(final byte[] imageBytes) {
    final long start = System.currentTimeMillis();

    try (final Tensor image = this.normalizedImageToTensor(imageBytes)) {

      final float[] labelProbabilities = this.classifyImageProbabilities(image);

      final int bestLabelIdx = this.maxIndex(labelProbabilities);

      final ClassificationSpec classificationSpec =
          new ClassificationSpec(
              this.labels.get(bestLabelIdx),
              labelProbabilities[bestLabelIdx] * 100f,
              System.currentTimeMillis() - start);

      this.log.debug(
          String.format(
              "Image classification [%s %.2f%%] took %d ms",
              classificationSpec.getLabel(),
              classificationSpec.getProbability(),
              classificationSpec.getElapsed()));

      log.info(classificationSpec.toString());
      return classificationSpec;
    }
  }

  @Override
  public float[] classifyImageProbabilities(final Tensor image) {
    try (final Tensor result =
        this.session.runner().feed("input", image).fetch(this.outputLayer).run().get(0)) {
      final long[] rshape = result.shape();
      if (result.numDimensions() != 2 || rshape[0] != 1) {
        throw new RuntimeException(
            String.format(
                "Expected model to produce a [1 N] shaped tensor where N is the number of labels, instead it produced one with shape %s",
                Arrays.toString(rshape)));
      }
      final int nlabels = (int) rshape[1];
      return ((float[][]) result.copyTo(new float[1][nlabels]))[0];
    }
  }

  @Override
  public int maxIndex(final float[] probabilities) {
    int best = 0;
    for (int i = 1; i < probabilities.length; ++i) {
      if (probabilities[i] > probabilities[best]) {
        best = i;
      }
    }
    return best;
  }

  @Override
  public Tensor normalizedImageToTensor(final byte[] imageBytes) {
    try (final Graph g = new Graph()) {
      final GraphBuilder b = new GraphBuilder(g);
      final Output input = b.constant("input", imageBytes);
      final Output output =
          b.div(
              b.sub(
                  b.resizeBilinear(
                      b.expandDims(
                          b.cast(b.decodeJpeg(input, 3), DataType.FLOAT),
                          b.constant("make_batch", 0)),
                      b.constant("size", new int[] {this.height, this.width})),
                  b.constant("mean", this.mean)),
              b.constant("scale", this.scale));
      try (final Session s = new Session(g)) {
        return s.runner().fetch(output.op().name()).run().get(0);
      }
    }
  }

  @PreDestroy
  public void close() {
    this.session.close();
  }
}
