package com.stefanomantini.service.contract;

public interface ApiTraceService {

  String logRequest(
      Direction direction,
      String method,
      String requestUrl,
      String correlationId,
      String body,
      String traceId);

  void logResponse(
      Direction direction,
      String requestLoggingId,
      String method,
      String requestUrl,
      String correlationId,
      String body,
      Integer responseStatus);

  enum Direction {
    INBOUND,
    OUTBOUND
  }
}
