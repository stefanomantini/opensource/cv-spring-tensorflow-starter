package com.stefanomantini.service.contract;

import com.stefanomantini.service.model.ClassificationSpec;
import org.tensorflow.Tensor;

public interface ClassificationService {

  /**
   * Runs classification and maps to DTO
   *
   * @param imageBytes
   * @return
   */
  ClassificationSpec classifyImage(byte[] imageBytes);

  /**
   * Runs probabilities
   *
   * @param image
   * @return float array of probabilities
   */
  float[] classifyImageProbabilities(Tensor image);

  /**
   * @param probabilities
   * @return highest probability index from array
   */
  int maxIndex(float[] probabilities);

  /**
   * The model was trained with images scaled to 299x299 pixels. The colors, represented as R, G, B
   * in 1-byte each were converted to float using (value - Mean)/Scale. Since the graph is being
   * constructed once per execution here, we can use a constant for the input image. If the graph
   * were to be re-used for multiple input images, a placeholder would have been more appropriate.
   *
   * @param imageBytes
   * @return
   */
  Tensor normalizedImageToTensor(byte[] imageBytes);
}
