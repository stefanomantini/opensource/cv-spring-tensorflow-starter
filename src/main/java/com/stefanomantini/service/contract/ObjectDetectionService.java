package com.stefanomantini.service.contract;

public interface ObjectDetectionService {

  byte[] detectObjects(byte[] fileToAnalyse);
}
