package com.stefanomantini.service.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClassificationSpec {
  private String label;
  private float probability;
  private long elapsed;
}
