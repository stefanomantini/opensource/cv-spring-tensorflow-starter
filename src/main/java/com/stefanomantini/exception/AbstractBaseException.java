package com.stefanomantini.exception;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(callSuper = true)
public abstract class AbstractBaseException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  private final ErrorCode errorCode;

  public AbstractBaseException(final ErrorCode errorCode, final String message) {
    super(message);
    this.errorCode = errorCode;
  }
}
