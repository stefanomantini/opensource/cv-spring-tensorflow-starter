package com.stefanomantini.exception;

public class GenericException extends AbstractBaseException {

  public GenericException(final Throwable cause) {
    super(
        ErrorCode.UNHANDLED,
        String.join(ErrorCode.UNHANDLED.name(), cause.getClass().toString(), cause.getMessage()));
  }
}
