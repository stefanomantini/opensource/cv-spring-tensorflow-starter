package com.stefanomantini.exception;

public enum ErrorCode {
  UNHANDLED("Unhandled Exception"),
  INVALID_MODEL("Model File is Invalid"),
  INVALID_LABELS("Labels File is Invalid");

  ErrorCode(final String s) {}
}
