package com.stefanomantini.config;

import com.stefanomantini.exception.ErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.tensorflow.Graph;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class ModelConfiguration extends SpringBootServletInitializer {

  @Bean
  public Graph tfModelGraph(@Value("${tf.frozenModelPath}") final String tfFrozenModelPath)
      throws ConfigurationException {
    final Resource graphResource = this.getResource(tfFrozenModelPath);

    final Graph graph = new Graph();
    try {
      graph.importGraphDef(IOUtils.toByteArray(graphResource.getInputStream()));
    } catch (final IOException e) {
      throw new ConfigurationException(ErrorCode.INVALID_MODEL, e.getCause());
    }
    log.info("Loaded Tensorflow model: {}", tfFrozenModelPath);
    return graph;
  }

  private Resource getResource(@Value("${tf.frozenModelPath}") final String tfFrozenModelPath) {
    Resource graphResource = new FileSystemResource(tfFrozenModelPath);
    if (!graphResource.exists()) {
      graphResource = new ClassPathResource(tfFrozenModelPath);
    }
    if (!graphResource.exists()) {
      throw new ConfigurationException(ErrorCode.INVALID_MODEL);
    }
    return graphResource;
  }

  @Bean
  public List<String> tfModelLabels(@Value("${tf.labelsPath}") final String labelsPath) {
    final Resource labelsRes = this.getResource(labelsPath);
    log.info("Loaded model labels: {}", labelsPath);
    try {
      return IOUtils.readLines(labelsRes.getInputStream(), Charset.forName("UTF-8")).stream()
          .map(label -> label.substring(label.contains(":") ? label.indexOf(':') + 1 : 0))
          .collect(Collectors.toList());
    } catch (final IOException e) {
      throw new ConfigurationException(ErrorCode.INVALID_LABELS, e.getCause());
    }
  }
}
