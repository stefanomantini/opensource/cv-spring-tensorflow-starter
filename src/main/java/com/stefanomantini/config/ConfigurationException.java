package com.stefanomantini.config;

import com.stefanomantini.exception.AbstractBaseException;
import com.stefanomantini.exception.ErrorCode;

class ConfigurationException extends AbstractBaseException {

  private static final long serialVersionUID = 1L;

  ConfigurationException(final ErrorCode errorCode, final Throwable cause) {
    super(
        errorCode,
        String.join(errorCode.toString(), cause.getClass().toString(), cause.getMessage()));
  }

  ConfigurationException(final ErrorCode errorCode) {
    super(errorCode, errorCode.toString());
  }
}
