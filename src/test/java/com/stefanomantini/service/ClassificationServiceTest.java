package com.stefanomantini.service;

import com.stefanomantini.service.contract.ClassificationService;
import com.stefanomantini.service.model.ClassificationSpec;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ClassificationServiceTest {

  @Autowired private ResourceLoader resourceLoader;

  @Autowired private ClassificationService classificationService;

  @Test
  public void classifyServiceMultipleImages() {

    final String[] images = new String[] {"car.png", "boat.jpg", "peach.jpg"};

    final List<ClassificationSpec> classifiers =
        Arrays.stream(images)
            .map(
                image -> {
                  try {
                    byte[] bytes =
                        Files.readAllBytes(
                            Paths.get(
                                this.resourceLoader
                                    .getResource("classpath:images/" + image)
                                    .getURI()));

                    ClassificationSpec classificationSpec =
                        this.classificationService.classifyImage(bytes);

                    Assert.assertNotNull(classificationSpec.getLabel());

                    return classificationSpec;

                  } catch (IOException e) {
                    throw new IllegalArgumentException(e);
                  }
                })
            .collect(Collectors.toList());

    classifiers.forEach(c -> log.info("Labelled {} in {} ms", c.getLabel(), c.getElapsed()));
  }
}
