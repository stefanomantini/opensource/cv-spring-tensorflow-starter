package com.stefanomantini.api;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ResourceLoader;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.file.Files;
import java.nio.file.Paths;

import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class ClassificationControllerTest {

  @Autowired private MockMvc mockMvc;

  @Autowired private ResourceLoader resourceLoader;

  @Test
  public void classifyImageOk() throws Exception {

    final byte[] bytes =
        Files.readAllBytes(
            Paths.get(resourceLoader.getResource("classpath:images/boat.jpg").getURI()));

    final MockMultipartFile file =
        new MockMultipartFile("file", "images/boat.jpg", "image/jpeg", bytes);

    this.mockMvc
        .perform(multipart("/api/classify").file(file))
        .andExpect(status().isOk())
        .andExpect(jsonPath("label", notNullValue()))
        .andExpect(jsonPath("probability", notNullValue()))
        .andReturn()
        .getResponse()
        .getContentAsString();
  }
}
