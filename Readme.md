## Computer Vision with Spring Boot and Tensorflow Starter

#### Getting started
* Prerequisites:
    * JDK8+
* Running the API
    * ```./gradlew fetchInceptionFrozenModel bootrun```
    * http://localhost:8080 